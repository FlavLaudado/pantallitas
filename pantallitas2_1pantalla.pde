/*
Pantallitas
 Guión de 42 escenas
 morse, texto, lineas, negro, sonido, 
 fc 800hz / 700hz, entre 400 y 1000hz
 
 AGREGAR:
 *exportar video con sonido
 Cambiar cómo agarar el texto...
 *char c1 = str.charAt(0);
 
 SEGUIR: con escena 35!!
 en "textoM" no se reproduce el último renglón... por qué no entra?
 -todos los "textoM" hasta e 17 agregarles un renglón vacío, y agregarles 1 en nMorse[]
 -rechequear "negro", no entiendo los tiempos
 */

import processing.sound.*;
SinOsc sine;

import com.hamoid.*;
VideoExport videoExport;

PFont courier;

color fondo;
color negro = color(0);
color blanco = color(255);
//color c[] = { };

int dx = 80;
int dy = 48;
int f  = 18;//24;
int px[] = { f, f*2 + dx, f*3 + dx*2, f*4 + dx*3, f*5 + dx*4, f*6 + dx*5, f*7 + dx*6, f*8 + dx*7};
int py;
int cantPantallitas = 8;
//armar una claqueta con la escena 0, un rect rojo con un borde azul
//claqueta, 1, 17
//String escenas[] = {"claqueta", "morseS", "textoS"};
String escenas[] = {"claqueta", "morseS", "textoS", "negro", "morse", "textoM", "textoS", "morseL", "texto", "negro", "texto", 
  "sonido", "negro", "texto", "morseL", "textoM", "textoM", "textoS", "textoM", "textoS", 
  "negro", "textoM", "negro", "textoS", "morse", "textoS", "negro", "textoS", "textoM", "textoS", 
  "textoM", "textoM", "textoM", "morse", "morse", "negro", "morse", "morse", "textoS", "morse", 
  "morse", "morse", "morse", "morse", "morse"}; 
int nEscena = 44;
int escenaTotal = 44;//12;
int nPantallitas = 8; //si es 8 se ven las 8 pantallitas
//tiempo de escena ...
//sacar el largo de cada String en el setup y completar este array 
int nMorse[] = {2, 20, 20, 10, 20, 3, 40, 20, 2, 2, 
  2, 20, 2, 2, 20, 4, 6, 45, 4, 9, 
  10, 12, 10, 124, 17, 87, 10, 98, 3, 78, 
  3, 2, 2, 41, 45, 3, 38, 33, 74, 31, 
  31, 31, 59, 47, 15};

String morse1S = " .- .-. .. -.. . --.. / -. --- / -.. .. -.-. . & . .-.. / -.-. ..- .-.. .- - .- --.. --- / . -. -.-. .- .--. ... ..- .-.. .- -.. --- & . -. / -.-. .- -.. .- / --. .-. .- -. --- & .. . / ... .- .-..";

//int nMorse[] = {91, -, 0, };
//int nMorse1 = //91;
int n = 0;
int m = 3; 
int flip = 0;//una moneda de dos caras para entrar o no al array de morse

int t1  = 100;
long timeOn = t1;
long previousMillis = 0;
int t10 = t1*10;//10000;
int timeTexto = t1*50; //t1*50;

String textosM[] = {" ", " .- .-. .. -.. . --.. / -. --- / -.. .. -.-. . & . .-.. / -.-. ..- .-.. .- - .- --.. --- / . -. -.-. .- .--. ... ..- .-.. .- -.. --- & . -. / -.-. .- -.. .- / --. .-. .- -. --- & .. . / ... .- .-..", ".- .-. .. -.. . --.. / -. --- / -.. .. -.-. . / . .-.. / -.-. ..- .-.. .- - .- --.. --- / . -. -.-. .- .--. ... ..- .-.. .- -.. --- / . -. / -.-. .- -.. .- / --. .-. .- -. --- / -.. . / ... .- .-.. .-.-. ", "", ".- .-. .. -.. . --.. / --. --- .-.. .--. . .- / . -. / . .-.. / .-.. . -. --. ..- .- .--- .//.--. . .-. -.-. ..- ... .. --- -. / .- .-. .-. .. - -- .. -.-. .-//-.. . / ..- -. / .. -. -.-. . -. -.. .. ---//. -. / . ... - .- -.. --- / .--. ..- .-. ---.- .-. -.. . / . -. / - .-. . -.-. .... --- ... / -.. . / -.-. .- .-. -. .//.--. . - .-. . .-//... --- -... .-. . / . .-.. / -- .- .--. .- / .. -. ...- .. ... .. -... .-.. .//-.. . .-.. / -- .- .-.", "", ".- .-. .. -.. . --.. / -.. . ... .- .--. .- .-. . -.-. . / . -. / .-.. --- ... / .--. --- .-. --- ... / // -.. . .-.. / .- .. .-. . .-.-.", ".- ...- .- -. --.. .- / .--. --- .-. / ..- -. / ... ..- .-. -.-. ---//-.. . / -- .. . -.. ---//.-. ..- -- -... --- / .- .-.. / ... ..- .-.//- .-. .- ... .--. .- ... .-//.-.. .. -. . .- ... / ..-. .-. --- -. - . .-. .. --.. .- ...//-.. . .-.. / -- .- .--. .- / .--. --- .-.. .. - .. -.-. ---//--.- ..- . / ..- -. / .- .-.. ..- -- -. --- / -.. . / .-.. .. -.-. . ---//.- -. .. --- ... / -.. . ... .--. ..- . ...//-- .. .-. .- / -.-. --- -. / .. -. -.. .. ..-. . .-. . -. -.-. .. .-//... .. -. / .- .--. .-. . -. -.. . .-. / .-.. .- / .-.. . -.-. -.-. .. --- -.", "", "", 
  "", ". .--- . .-. -.-. .. -.-. .. --- ... / -.. . / .- .-. .. -.. . --..// . -. / .-.. .- / .... .. ... - --- .-. .. .-//-.. . / .-.. .- / -... .- -. -.. . .-. .-", "", "", "- . -. . -- --- ... / . .-.. / -.. . ... .. . .-. - ---//-- .- ... / .- .-. .. -.. ---//-.. . .-.. / .--. .-.. .- -. . - .- / -.. .. --. .- -. .-.. ---//-- ... / ..-. ..- . .-. - .", "", "", "--. .-. .- -... . -. ... . .-.. ---//-.-. --- -. / ..- -. / -.-. --- .-. ...- ---//.--. . - .-. .. ..-. .. -.-. .- -.. ---", "", "..-. --- .-. .- ... - . .-. ---", 
  "", "", "", ".- .-. .. -.. . --.. ....... -.-. --- -- --- ....... . .-.. ....... ... ..- . .-.. -.. ---/-.. . ....... -.-. .... .. .-.. .", ".-. .- .--. .. -.. ---", ". .-.. ....... -- . - .- .-.. ....... - .-. .- -. --.- ..- .. .-.. ---/-.. . ....... -- .. ....... ...- --- --..", "", "... . ....... . -..- - .. . -. -.. . ....... -.-. --- -- --- ....... -- .- -. - ---/.- .-. .. -.. . --.. ....... .... .. . -. -.. .", "", "-- .- .--. .- ....... ..-. ... .. -.-. --- ....... -- .- .--. .- ....... .--. --- .-.. - .. -.-. ---", 
  "", "", "", ".-.. .- ....... ... .. .-.. ..- . - .- ....... -. ...- . .-", "-.. . ....... . ... - . ....... -.-. --- .-. ...- ---", "", "... .. . -- .--. .-. . / ...- . -. -.-. . -.. --- .-.", ".--- .- -- .- ... / ...- . -. -.-. .. -.. ---", "... ..- / . -- .--. ..- -. .. .- -.. ..- .-. .- / -.. . / -... .-. --- -. -.-. . / .- .-.. .. .- -.. ---", "-... .-. .. .-.. .-.. .- -. - . / .- ..- -.", 
  "-.-. ..- .- -. -.. --- / - .. .-. .. - .- -.", ". -. ... .- -. --. .-. . -. - .- -.. --- ...", ".-.. --- ... / -.-. ..- . .-. .--. --- ... / -.-- / -.. . ... .--. ..- -. - .- -.", ".-.. . -. - --- ... / .--. . - .-. .. ..-. .. -.-. .- -.. --- ...", "--. .-. .. - --- ..."};

String textos[] = {" ", "Aridez no dice     el culatazo encapsulado     en cada grano     de sal", "Aridez no dice     el culatazo encapsulado     en cada grano     de sal", " ", " Aridez golpea en el lenguaje percusión arrítmica de un incendio en estado puro arde en trechos de carne pétrea sobre el mapa invisible del mar", " ", "Aridez desaparece en los poros      del aire", "avanza por un surco de miedo rumbo al sur traspasa líneas fronterizas del mapa político que un alumno de liceo años después mira con indiferencia sin aprender la lección", "Aridez como pausa     o silencio corto", "", 
  "ejercicios de aridez     en la historia      de la bandera", "ejercicios de aridez en la historia de la bandera", "", "Aridez como mito     indestructible     aprendan de una vez      la lección", "tenemos el desierto más árido del planeta díganlo más fuerte", "tenemos el desierto más árido del planeta díganlo más fuerte", "saquen pecho  izando la bandera  no estimen  en elogios  tenemos el desierto  más árido", "grábenselo  con un corvo  petrificado", "", "forastero", 
  "", "", "", "Aridez como el sueldo / de Chile", "   Rápido Reapido", "el metal tranquilo         de mi voz", "", "se extiende como manto    aridez hiende", "rasga la carne de la tierra  en dos", "mapa físico    mapa político", 
  "a ambos los habita  a ambos los disuelve", "no olvides jamás olvides       ni en tus peores resentidos      episodios", "ni en el exilio relativo    de la noche", "la silueta nívea", "  de este corvo", "", "siempre vencedor", "jamás vencido", "su empuñadura de bronce aliado", "brillante aún", 
  "cuando tiritan", "ensangrentados", "los cuerpos y despuntan", "lentos petrificados", "gritos"};

// textos "textoS" -> simples
String texto2[] = {" Aridez", " no dice", "el culatazo", "encapsulado", "    en cada", "grano", "        de", "sal"};
String texto6[] = {" Aridez", " desaparece", "    en", "    los", "  poros", " ", "     del", " aire"};
String texto19[] = {"forastero", "forastero", "forastero", "forastero", "forastero", "forastero", "forastero", "forastero"};
String texto24[] = {" ", " ", " ", "Rápido", "Rápido", " ", " ", " "};
String texto29[] = {" ", " ", "  mapa", " físico", "  mapa", " político", " ", " "};
String texto31[] = {"    no", " olvides", " jamas", " olvides", " ni en tus ", "  peores", "resentidos", " episodios"};
String texto32[] = {"    ni", "    en", "    el", "  exilio", " relativo", "    de", "    la", "   noche"};
String texto33[] = {" ", " ", " ", "la silueta", "   nívea", " ", " ", " "};
String texto34[] = {" ", " ", " ", " de este", "  corvo", " ", " ", " "};
String texto36[] = {" ", " ", " ", " siempre", " vencedor", " ", " ", " "};
String texto37[] = {" ", " ", " ", " jamás", " vencido", " ", " ", " "};
String texto38[] = {" ", " ", "    su", "empuñadura", "de bronce", " aliado", " ", " "};
String texto39[] = {" ", " ", "", "brillante", "  aún", " ", " ", " "};
String texto40[] = {" ", " ", "", "  cuando", " tiritan", " ", " ", " "};
String texto41[] = {" ", " ", "", "ensangren", "tados", " ", " ", " "};
String texto42[] = {" ", " ", "", "los cuerpos", "y despuntan", " ", " ", " "};
String texto43[] = {" ", " ", "", "  lentos", "petrificados", " ", " ", " "};
String texto44[] = {" ", " ", "", " gritos", " gritos", " ", " ", " "};

String texto5 = "Aridez golpea en el lenguaje     percusión arrítmica de un incendio    en estado puro     arde en trechos de carne pétrea     sobre el mapa invisible      del mar";
String texto5b[] = {"Aridez golpea en el lenguaje     percusión arrítmica", "de un incendio      en estado puro      arde en trechos de carne", "pétrea      sobre el mapa invisible      del mar", ""};
int n5 = 0;
//String texto6 = "Aridez desaparece en los poros      del aire";
String texto8 = "Aridez como pausa     o silencio corto";
String texto15[] = {"tenemos el     desierto", "más     árido", "del planeta     díganlo", "más     fuerte"};
//palabra más larga 9 carqacteres "arrítmica"
String[][] textos5 = {  {"", "Aridez", "golpea", "en el", " lenguaje", "percusión", "arrítmica", ""}, 
  {"    de un", "incendio", "en estado", "puro", "  arde en", " trechos", "de carne", ""}, 
  {"pétrea", "", "   sobre", " el mapa", "invisible", "     del", "mar", ""} };

String[][] textos15 = {  
  {"", "", "", "tenemos el", "desierto", "", "", ""}, 
  {"", "", "", "  más", "árido", "", "", ""}, 
  {"", "", "", "del planeta", "díganlo", "", "", ""}, 
  {"", "", "", "  más", " fuerte", "", "", ""} };

String[][] textos16 = {  
  {"", "", "", " saquen", " pecho", "", "", ""}, 
  {"", "", "", "izando la", "bandera", "", "", ""}, 
  {"", "", "", "   no", "escatimen", "", "", ""}, 
  {"", "", "", "   en", "elogios", "", "", ""}, 
  {"", "", "", "tenemos el", "desierto", "", "", ""}, 
  {"", "", "", "   más", " árido", "", "", ""} };

String[][] textos17 = {  
  {"", "", "", "   gráben", "selo", "", "", ""}, 
  {"", "", "", "  con un", " corvo", "", "", ""}, 
  {"", "", "", "    petri", "ficado", "", "", ""} };
int n17[] = { 10, 20, 30}; 
int nTexto17 = 0;
String[][] textos18 = {  
  {"", "", "", "    el", " amigo", "", "", ""}, 
  {"", "", "", "  cuando", "es", "", "", ""}, 
  {"", "", "", "forastero", " ", "", "", ""}, 
  {"", "", "", " ", " ", "", "", ""} };
int n18[] = { 10, 20, 30}; 
int nTexto18 = 0;
int nOtra = 0;
String[][] textos21 = {  
  {"", "", "", " Aridez", " quieta", "", "", ""}, 
  {"", "", "", "   del", " campo", "", "", ""}, 
  {"", "", "", "    de", "concentración", "", "", ""}, 
  {"", "", "", "   ojo", " de sal", "", "", ""}, 
  {"", "", "", "abriendo sus", " párpados", "", "", ""}, 
  {"", "", "", " al soplo", " nocturno", "", "", ""}, 
  {"", "", "", "   del", " desierto", "", "", ""}, 
  {"", "", "", " maldita", " la hora", "", "", ""}, 
  {"", "", "", " piensa el", " coronel", "", "", ""}, 
  {"", "", "", " maldita", " la ley", "", "", ""}, 
  {"", "", "", "piensa el", " confinado", "", "", ""}, 
  {"", "", "", " ", " ", "", "", ""} };

String[][] textos23 = {  
  {"", "", " Aridez", "  como", "   el", "  sueldo", "", ""}, 
  {"", "", "", "    de", " Chile", "", "", ""}, 
  {"", "", "", "    ", "", "", "", ""} };
int n23[] = { 90, 20}; 
int nTexto23 = 0;

String[][] texto25 = {
  {" ", " ", " ", "el metal", "tranquilo", " ", " ", " "}, 
  {"", "", "", "   de mi", " voz", "", "", ""}, 
  {"", "", "", " ", " ", "", "", ""} };
String[][] texto27 = {
  {" ", " ", "    se", " extiende", "  como", "  manto", " ", " "}, 
  {"", "", "", "  aridez", "   hiende", "", "", ""}, 
  {"", "", "", " ", " ", "", "", ""} };
String[][] textos28 = {  
  {"", "", "rasga la", "  carne", "  de la ", "  tierra", "", ""}, 
  {"", "", "", "   en", "   dos", "", "", ""}, 
  {"", "", "", " ", " ", "", "", ""} };
String[][] textos30 = {  
  {"", "", "   a", "  ambos", "   los ", " habita", "", ""}, 
  {"", "", "   a", "  ambos", "   los ", "disuelve", "", ""}, 
  {"", "", "", " ", " ", "", "", ""} };

int textoSizeB = 140;//155;//texto a pantalla completa
int textoSizeS = 15;//texto pantallitas
int textX = textoSizeB / 2;

void setup() {
  fondo = color(75);
  //fondo = color(0);
  background(fondo);
  size(800, 480);
  //size(880, 440);
  //fullScreen();

  py = height/2 - dy/2;
  courier = createFont("Courier", textoSizeB);

  sine = new SinOsc(this);
  sine.freq(1000);
  sine.amp(0.1);

  //videoExport = new VideoExport(this);
  //videoExport.startMovie();
}

void draw() {
  textFont(courier);

  if (millis() - previousMillis >= timeOn) {
    previousMillis = millis();

    if (escenas[nEscena] == "morseS") {//ex 1
      if (flip == 1) {
        escribirEscena(nEscena);
        escribirCaracter( textos[nEscena].charAt(n));
        verEscucharMorse( textosM[nEscena].charAt(n));
        actualizarContador();
      } else {
        escribirCaracter( '*');
        verEscucharMorse( '*');//espacio negro
        flip = 1;
      }    
      dibujarPantallitas();
    }
    if (escenas[nEscena] == "textoS") {//texto a lo largo + sonido
      background(fondo);
      escribirEscena(nEscena);
      fill(negro);
      dibujarPantallitas();
      fill(255);
      if (nEscena  == 2) {   
        if (nPantallitas == 8) {
          textSize(textoSizeS - 2.7); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(texto2[i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB - 30); 
          text(texto2[nPantallitas], 10, py + textX);
        }
      }
      if (nEscena  == 6) {   
        if (nPantallitas == 8) {
          textSize(textoSizeS - 2.7); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(texto6[i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB - 30); 
          text(texto6[nPantallitas], 10, py + textX);
        }
      }
      if (nEscena  == 17) {      //escribo el texto
        if (n >= n17[0]) {
          nOtra = 1;
        } 
        if (n >= n17[1]) {
          nOtra = 2;
        } 
        if (nPantallitas == 8) {
          textSize(textoSizeS); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(textos17[nOtra][i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB); 
          text(textos17[nOtra][m], 10, py + textX);
        }
      }
      if (nEscena  == 19) {   
        if (nPantallitas == 8) {
          textSize(textoSizeS ); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(texto19[i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB ); 
          text(texto19[nPantallitas], 10, py + textX);
        }
      }
      if (nEscena  == 23) {      //escribo el texto
        if (n >= n23[0]) {
          nOtra = 1;
        } 
        if (nPantallitas == 8) {
          textSize(textoSizeS); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(textos23[nOtra][i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB); 
          text(textos23[nOtra][m], 10, py + textX);
        }
      }
      if (nEscena  == 25) { 
        if (n >= 53) {
          nOtra = 1;
        }
        if (nPantallitas == 8) {
          textSize(textoSizeS - 2.7); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(texto25[nOtra][i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB - 30); 
          text(texto25[nOtra][m], 10, py + textX);
        }
      }
      if (nEscena  == 27) { 
        if (n >= 62) {
          nOtra = 1;
        }
        if (nPantallitas == 8) {
          textSize(textoSizeS - 2.7); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(texto27[nOtra][i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB - 30); 
          text(texto27[nOtra][m], 10, py + textX);
        }
      }
      if (nEscena  == 29) {   
        if (nPantallitas == 8) {
          textSize(textoSizeS ); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(texto29[i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB ); 
          text(texto29[nPantallitas], 10, py + textX);
        }
      }
      if (nEscena  == 38) {   
        if (nPantallitas == 8) {
          textSize(textoSizeS - 1); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(texto38[i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB - 10); 
          text(texto38[nPantallitas], 10, py + textX);
        }
      }
      if (flip == 1) {
        //background(fondo);
        //escribirEscena(nEscena);
        //escribirCaracter( textos[nEscena].charAt(n)); 
        escucharMorse( textosM[nEscena].charAt(n));
        actualizarContador();
      } else {
        //escribirCaracter( '*');
        escucharMorse( '*');//espacio negro
        flip = 1;
      }
      //dibujarPantallitas();
    }

    if (escenas[nEscena] == "negro") {//ex 3
      escribirEscena(nEscena);
      timeOn = t10;
      //pantalla a negro
      fill(negro);
      dibujarPantallitas();

      actualizarContador();
    }
    if (escenas[nEscena] == "claqueta") {
      escribirEscena(nEscena);
      timeOn = t10;
      //pantalla a negro
      strokeWeight(8);
      stroke(0, 0, 255);
      fill(255, 0, 0);
      rect( 0, 0, width, height);
      //dibujarPantallitas();
      actualizarContador();
    }
    if (escenas[nEscena] == "morse") {//ex 4
      if (flip == 1) {
        escribirEscena(nEscena);
        //escribirCaracter( textos[nEscena].charAt(n));
        verMorse( textosM[nEscena].charAt(n));
        actualizarContador();
      } else {
        escribirCaracter( '*');
        verEscucharMorse( '*');//espacio negro
        flip = 1;
      }
      dibujarPantallitas();

      if (nEscena  == 24) { //pantalla 3 y 4 texto
        if (nPantallitas == 8) {
          for (int i = 3; i < 5; i++) {
            if (textos[nEscena] != " ") {
              fill(0);
              rect( px[i], py, dx, dy);
              fill(255);
              textSize(textoSizeS + 4.5); 
              text(texto24[i], px[i], py + dy*0.6);
            }
          }
        } else { 
          if (nPantallitas == 3 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB + 45); 
            text(texto24[nPantallitas], f, py + textX);
          }
          if (nPantallitas == 4 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB + 44); 
            text(texto24[nPantallitas], f, py + textX);
          }
        }
      }
      if (nEscena  == 33) { 
        if (nPantallitas == 8) {
          for (int i = 3; i < 5; i++) {
            if (textos[nEscena] != " ") {
              fill(0);
              rect( px[i], py, dx, dy);
              fill(255);
              textSize(textoSizeS - 1.5); 
              text(texto33[i], px[i], py + dy*0.6);
            }
          }
        } else { 
          if (nPantallitas == 3 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB - 15); 
            text(texto33[nPantallitas], f, py + textX);
          }
          if (nPantallitas == 4 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB - 15); 
            text(texto33[nPantallitas], f, py + textX);
          }
        }
      }
      if (nEscena  == 34) {
        if (nPantallitas == 8) {
          for (int i = 3; i < 5; i++) {
            if (textos[nEscena] != " ") {
              fill(0);
              rect( px[i], py, dx, dy);
              fill(255);
              textSize(textoSizeS - 1.5); 
              text(texto34[i], px[i], py + dy*0.6);
            }
          }
        } else { 
          if (nPantallitas == 3 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB - 15); 
            text(texto34[nPantallitas], f, py + textX);
          }
          if (nPantallitas == 4 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB -15); 
            text(texto34[nPantallitas], f, py + textX);
          }
        }
      }
      if (nEscena  == 36) {
        if (nPantallitas == 8) {
          for (int i = 3; i < 5; i++) {
            if (textos[nEscena] != " ") {
              fill(0);
              rect( px[i], py, dx, dy);
              fill(255);
              textSize(textoSizeS); 
              text(texto36[i], px[i], py + dy*0.6);
            }
          }
        } else { 
          if (nPantallitas == 3 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB); 
            text(texto36[nPantallitas], f, py + textX);
          }
          if (nPantallitas == 4 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB); 
            text(texto36[nPantallitas], f, py + textX);
          }
        }
      }
      if (nEscena  == 37) {
        if (nPantallitas == 8) {
          for (int i = 3; i < 5; i++) {
            if (textos[nEscena] != " ") {
              fill(0);
              rect( px[i], py, dx, dy);
              fill(255);
              textSize(textoSizeS); 
              text(texto37[i], px[i], py + dy*0.6);
            }
          }
        } else { 
          if (nPantallitas == 3 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB); 
            text(texto37[nPantallitas], f, py + textX);
          }
          if (nPantallitas == 4 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB); 
            text(texto37[nPantallitas], f, py + textX);
          }
        }
      }
      if (nEscena  == 39) {
        if (nPantallitas == 8) {
          for (int i = 3; i < 5; i++) {
            if (textos[nEscena] != " ") {
              fill(0);
              rect( px[i], py, dx, dy);
              fill(255);
              textSize(textoSizeS ); 
              text(texto39[i], px[i], py + dy*0.6);
            }
          }
        } else { 
          if (nPantallitas == 3 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB); 
            text(texto39[nPantallitas], f, py + textX);
          }
          if (nPantallitas == 4 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB); 
            text(texto39[nPantallitas], f, py + textX);
          }
        }
      }
      if (nEscena  == 40) {
        if (nPantallitas == 8) {
          for (int i = 3; i < 5; i++) {
            if (textos[nEscena] != " ") {
              fill(0);
              rect( px[i], py, dx, dy);
              fill(255);
              textSize(textoSizeS); 
              text(texto40[i], px[i], py + dy*0.6);
            }
          }
        } else { 
          if (nPantallitas == 3 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB); 
            text(texto40[nPantallitas], f, py + textX);
          }
          if (nPantallitas == 4 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB); 
            text(texto40[nPantallitas], f, py + textX);
          }
        }
      }
      if (nEscena  == 41) {
        if (nPantallitas == 8) {
          for (int i = 3; i < 5; i++) {
            if (textos[nEscena] != " ") {
              fill(0);
              rect( px[i], py, dx, dy);
              fill(255);
              textSize(textoSizeS); 
              text(texto41[i], px[i], py + dy*0.6);
            }
          }
        } else { 
          if (nPantallitas == 3 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB); 
            text(texto41[nPantallitas], f, py + textX);
          }
          if (nPantallitas == 4 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB); 
            text(texto41[nPantallitas], f, py + textX);
          }
        }
      }
      if (nEscena  == 42) {
        if (nPantallitas == 8) {
          for (int i = 3; i < 5; i++) {
            if (textos[nEscena] != " ") {
              fill(0);
              rect( px[i], py, dx, dy);
              fill(255);
              textSize(textoSizeS - 2.5); 
              text(texto42[i], px[i], py + dy*0.6);
            }
          }
        } else { 
          if (nPantallitas == 3 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB - 25); 
            text(texto42[nPantallitas], f, py + textX);
          }
          if (nPantallitas == 4 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB - 25); 
            text(texto42[nPantallitas], f, py + textX);
          }
        }
      }
      if (nEscena  == 43) {
        if (nPantallitas == 8) {
          for (int i = 3; i < 5; i++) {
            if (textos[nEscena] != " ") {
              fill(0);
              rect( px[i], py, dx, dy);
              fill(255);
              textSize(textoSizeS - 3.2); 
              text(texto43[i], px[i], py + dy*0.6);
            }
          }
        } else { 
          if (nPantallitas == 3 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB - 32); 
            text(texto43[nPantallitas], f, py + textX);
          }
          if (nPantallitas == 4 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB - 32); 
            text(texto43[nPantallitas], f, py + textX);
          }
        }
      }
      if (nEscena  == 44) {
        if (nPantallitas == 8) {
          for (int i = 3; i < 5; i++) {
            if (textos[nEscena] != " ") {
              fill(0);
              rect( px[i], py, dx, dy);
              fill(255);
              textSize(textoSizeS); 
              text(texto44[i], px[i], py + dy*0.6);
            }
          }
        } else { 
          if (nPantallitas == 3 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB); 
            text(texto44[nPantallitas], f, py + textX);
          }
          if (nPantallitas == 4 ) {
            background(fondo);
            escribirEscena(nEscena);
            fill(negro);
            dibujarPantallitas();
            fill(255);
            textSize(textoSizeB ); 
            text(texto44[nPantallitas], f, py + textX);
          }
        }
      }
    }//final de "morse"

    if (escenas[nEscena] == "texto") {//ex 5
      background(fondo);
      escribirEscena(nEscena);
      fill(negro);
      dibujarPantallitas();
      textSize(textoSizeB); 
      fill(255);
      if (nPantallitas == 8) {
        textSize(textoSizeS); 
        text(textos[nEscena], f, py + textX);
      } else {
        textSize(textoSizeB); 
        //text(textos5[n][m], 10, py + textX);
        text(textos[nEscena], f, py + textX);
      }
      text(textos[nEscena], f, py + textX);
      timeOn = timeTexto;
      actualizarContador();
    }
    if (escenas[nEscena] == "textoM") {//texto múltiple
      background(fondo);
      escribirEscena(nEscena);
      fill(negro);
      dibujarPantallitas();
      fill(255);
      //agregar un switch case q según la escena agarre un txt diferente
      if (nEscena  == 5) {
        if (nPantallitas == 8) {
          textSize(textoSizeS); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(textos5[n][i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB); 
          text(textos5[n][m], 10, py + textX);
        }
      }
      if (nEscena  == 15) {
        if (nPantallitas == 8) {
          textSize(textoSizeS - 1.5); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(textos15[n][i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB - 17); 
          text(textos15[n][m], 10, py + textX);
        }
      }
      if (nEscena  == 16) {
        if (nPantallitas == 8) {
          textSize(textoSizeS - 3); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(textos16[n][i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB - 30); 
          text(textos16[n][m], 10, py + textX);
        }
      }
      if (nEscena  == 18) {
        if (nPantallitas == 8) {
          textSize(textoSizeS - 3); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(textos18[n][i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB - 30); 
          text(textos18[n][m], 10, py + textX);
        }
      }
      if (nEscena  == 21) {
        if (nPantallitas == 8) {
          textSize(textoSizeS - 5); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(textos21[n][i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB - 50); 
          text(textos21[n][m], 10, py + textX);
        }
      }
      if (nEscena  == 28) {
        if (nPantallitas == 8) {
          textSize(textoSizeS ); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(textos28[n][i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB ); 
          text(textos28[n][m], 10, py + textX);
        }
      }
      if (nEscena  == 30) {
        if (nPantallitas == 8) {
          textSize(textoSizeS ); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(textos30[n][i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB ); 
          text(textos30[n][m], 10, py + textX);
        }
      }
      if (nEscena  == 31) {   
        if (nPantallitas == 8) {
          textSize(textoSizeS - 3); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(texto31[i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB - 30); 
          text(texto31[nPantallitas], 10, py + textX);
        }
      }
      if (nEscena  == 32) {   
        if (nPantallitas == 8) {
          textSize(textoSizeS - 3); 
          for (int i = 0; i < cantPantallitas; i++) {
            text(texto32[i], px[i], py + dy*0.6);
          }
        } else {
          textSize(textoSizeB - 30); 
          text(texto32[nPantallitas], 10, py + textX);
        }
      }
      timeOn = timeTexto;
      actualizarContador();
    }
    if (escenas[nEscena] == "morseL") {//ex 7
      if (flip == 1) {
        background(fondo);
        escribirEscena(nEscena);
        escribirCaracter( textos[nEscena].charAt(n));
        actualizarContador();
      } else {
        escribirCaracter( '*');
        verEscucharMorse( '*');//espacio negro
        flip = 1;
      }

      if (nPantallitas == 8 && n != 0) {//está defasado en 1, si n=0 da error... volver a probar
        dibujarPantallitasNegro();
        verMorse( textosM[nEscena].charAt(n));
        dibujarLineas();
      } else {
        background(0);
        dibujarLinea();
      }
    }
    if (escenas[nEscena] == "sonido") {//11
      if (flip == 1) {
        background(fondo);
        escribirEscena(nEscena);
        escribirCaracter( textos[nEscena].charAt(n));
        escucharMorse( textosM[nEscena].charAt(n));
        fill(negro);
        actualizarContador();
      } else {
        escribirCaracter( '*');
        verEscucharMorse( '*');//espacio negro
        flip = 1;
      }
      dibujarPantallitas();
    }
  }

  //videoExport.saveFrame();
  //println("X: " + mouseX + ", y: " + mouseY);
  println(nEscena);
}

void escribirEscena(int numero) {
  textSize(12); 
  noStroke();
  fill(fondo);
  rect( 20, 20, 10, 12);
  fill( 255);
  text( numero, 22, 30);
}

void escribirCaracter(char _morse) {
  textSize(12); 
  noStroke();
  fill(fondo);
  rect( 20, 40, 10, 12);
  fill( 255);
  text(_morse, 22, 50);
}

void actualizarContador() {
  n++;
  flip = 0;
  if (n >= nMorse[nEscena]) {
    n = 0;
    //nOtra = 0;
    nEscena++;
    timeOn = t1;
    sine.stop();
    background(fondo);
    strokeWeight(1);
  }
  if (nEscena > escenaTotal) {
    nEscena = 0;
    //videoExport.endMovie();
    //exit();
  }
}
void dibujarPantallitas() {//futuro: va a tener que distinguir de un array q dibuja en c/pantalla
  if (nPantallitas == 8) {
    for (int i = 0; i < cantPantallitas; i++) {
      stroke(0);
      rect( px[i], py, dx, dy);
    }
  } else {
    m = nPantallitas;
    dibujarUnaPantalla();
  }
}
void dibujarUnaPantalla() {
  noStroke();
  rect( 0, 0, width, height);
}
void dibujarPantallitasNegro() {
  for (int i = 0; i < cantPantallitas; i++) {
    stroke(negro);
    fill(negro);
    rect( px[i], py, dx, dy);
  }
}
void dibujarLineas() {
  for (int i = 0; i < cantPantallitas; i++) {
    rect( px[i], py + dy*0.3, dx, dy*0.33);
  }
}
void dibujarLinea() {
  for (int i = 0; i < cantPantallitas; i++) {
    //stroke(0);
    rect( 0, 0.33 * height, width, 0.33 * height);
  }
}
void verEscucharMorse(char x) {
  if  (x == '.') {//un tiempo blanco
    fill(blanco);
    sine.play();
    timeOn = t1;
  }
  if  (x == '*') {//un tiempo negro
    fill(negro);
    sine.stop();
    timeOn = t1;
  }
  if  (x == '-') {//3 tiempos blanco
    fill(blanco);
    sine.play();
    timeOn = t1*3;
  }
  if  (x == ' ') {//3 tiempos negro
    fill(negro);
    sine.stop();
    timeOn = t1*3;
  }
  if  (x == '/') {//5 tiempos negro
    fill(negro);
    sine.stop();
    timeOn = t1*5;
  }
  if  (x == '&') {//10 tiempos negro
    fill(negro);
    sine.stop();
    timeOn = t1*10;
  }
}
void escucharMorse(char x) {
  if  (x == '.') {//un tiempo blanco
    //fill(blanco);
    sine.play();
    timeOn = t1;
  }
  if  (x == '*') {//un tiempo negro
    //fill(negro);
    sine.stop();
    timeOn = t1;
  }
  if  (x == '-') {//3 tiempos blanco
    //fill(blanco);
    sine.play();
    timeOn = t1*3;
  }
  if  (x == ' ') {//3 tiempos negro
    //fill(negro);
    sine.stop();
    timeOn = t1*3;
  }
  if  (x == '/') {//5 tiempos negro
    //fill(negro);
    sine.stop();
    timeOn = t1*5;
  }
}
void verMorse(char x) {
  if  (x == '.') {//un tiempo blanco
    fill(blanco);
    stroke(blanco);
    //sine.play();
    timeOn = t1;
  }
  if  (x == '*') {//un tiempo negro
    fill(negro);
    stroke(negro);
    //sine.stop();
    timeOn = t1;
  }
  if  (x == '-') {//3 tiempos blanco
    fill(blanco);
    stroke(blanco);
    //sine.play();
    timeOn = t1*3;
  }
  if  (x == ' ') {//3 tiempos negro
    fill(negro);
    stroke(negro);
    //sine.stop();
    timeOn = t1*3;
  }
  if  (x == '/') {//5 tiempos negro
    fill(negro);
    stroke(negro);
    //sine.stop();
    timeOn = t1*5;
  }
}
//void keyPressed() {
//  if (key == 'q') {
//    videoExport.endMovie();
//    exit();
//  }
//}
